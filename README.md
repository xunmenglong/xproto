# xproto简介

# 简介
一个读取速度比protobuf快100倍的序列化工具。

# 由来
笔者在实现一个自己的存储系统/搜索系统的时候，需要一个讲对象序列化为二进制的库，即将任意json转换为一段二进制。


将数据按照schema定义序列化后存储在磁盘上，然后再检索的时候再将遍历所有符合条件的数据并读取其中的字段进行筛选统计等操作。


基于如上场景，在实现过程中，我们期望有如下两点诉求：

1. 反序列化并读取其中某个字段的速度要求极高的性能，越快越好（最重要诉求）
1. 能够自解析，保证存储系统的兼容性，比如我在原来的schema当中增加了一个字段之后，可以不重新发布新版本就可以生效
1. 序列化后占据空间越小越好



笔者再考察 `protobuf` 和 `json` 之后，最终决定自己实现一个能同时满足如上2点需求的 `xproto` 的库，对比分析表格如下：



|  库  |  读取性能  |  是否支持自解析  |  占据空间  |
| --- | --- | --- | --- |
| protobuf | 快 | 不支持，需要重新编译 | 极小 |
| json | 慢 | 支持且方便 | 大 |
| xproto | 极快 | 支持 | 小 |



详细的性能和空间数据对比请本文最后一个章节：性能数据对比。

# 安装教程
## 依赖模块

1. xunmenlong/xutil
   1. 一些辅助函数库
   1. 地址：[https://gitee.com/xunmenglong/xutil](https://gitee.com/xunmenglong/xutil)
2. BRPC
   1. 其实只依赖rapidjson，直接复用brpc中的rapidjson了
   1. 地址：[https://gitee.com/baidu/BRPC](https://gitee.com/baidu/BRPC)
3. xxHash
   1. 将字符串计算签名转换为uint64
   1. 地址：[https://gitee.com/mirrors/xxHash](https://gitee.com/mirrors/xxHash)



## 安装依赖
工作跟目录下的目录结构如下：
```
.
|-- xunmenglong
|   | -- xutil
|   ` -- xproto
`-- third_party
    | -- BRPC
    ` -- xxHash
```


如果你相关依赖都安装好了，就直接跳过步骤，如果你现在还是个空目录，请参考如下命令安装好依赖。

如果你已经安装好了部分依赖，请参考注释将没装好的依赖安装好即可。

```shell
cd <your_root_dir>
mkdir third_party
cd third_party

# 安装和编译brpc
git clone https://gitee.com/baidu/BRPC.git
cd BRPC
sh config_brpc.sh --headers=/usr/include --libs=/usr/lib64
# 大概需要1分钟
make -j16

# 回到third_party目录
cd ..
# 安装和贬义xxHash
git clone https://gitee.com/mirrors/xxHash.git
cd xxHash
make

# 回到根目录，开始安装迅猛龙相关
cd ../..
mkdir xunmenglong
cd xunmenglong
# 下载xutil, xutil无需编译
git clone https://gitee.com/xunmenglong/xutil.git
```
## 安装xproto
参考如下命令：
```shell
# 下载xproto
git clone https://gitee.com/xunmenglong/xproto.git
## 编译xproto
cd xproto
make
## 编译示例
cd example
make
## 没有编译错误，看到日志输出即代表安装成功
./example
```
# 使用说明
## schema文件描述
跟 `protobuf` 类似，在使用 `xproto` 之前，必须先定义一下该结构体包含哪些字段，以及每个字段的类型，这个描述文件我们称之为 `schema` ，在 `xproto` 的实现中，schema是用json文件来描述的。


下面是一个最简单的例子：


数据json示例，包含几个简单字段：
```json
{
    "title": "阿富汗民众：感谢中国与我们站在一起",
    "url": "https://www.toutiao.com/i6902346346682909198/",
    "time": 1607077742
}
```
该数据json对应的schema描述文件：
```json
{
   "title": {
        "idx": 0,
        "type": "string",
        "is_option": 0
    },
    "url": {
        "idx": 1,
        "type": "string"
    },
    "time": {
        "idx": 2,
        "type": "int"
    }
}
```
该schema描述了一个包含三个字段的结构体，分别为 `title` 、`url` 、`time` 字段，每个字段包含三个描述信息：

1. idx：必选字段，类型为数字
   1. idx 字段特别重要，在序列化和读取数据的时候都依赖该字段来获取
   1. 该字段类似 `protobuf` 中针对每个字段的序号，即该字段在所有兄弟姐妹字段中排名第几
   1. 跟 `protobuf` 不一样的是，idx的起始值为0，而不是1，即第一个字段的 `idx` 为0
   1. 将来在序列化和读取只看idx的值，而不看字段在json中出现的先后顺序
   1. 所有兄弟节点的 `idx` 必须从0开始逐个连续递增，不能第一个字段写0，第二个字段写100，第三个字段写200
2. type：必选字段，类型为字符串
   1. 该字段的数据类型，取值为一个字符串，是一个枚举类型，支持如下几种类型的取值
   1. 普通字段：int/uint/int64/uint64/int8/uint8/int16/uint16/float/double/string
   1. 嵌套对象字段：object
      1. 如果是object字段，即还必须包含一个叫 `schema` 的字段，该字段递归描述了该object的下属字段类型
   4. 数组字段：array
      1. 如果是array字段，则还需要包含如下两个字段：
      1. element_type：取值范围同 `type` ，即该数组是由什么类型组成的
      1. 如果element_type是object的话，则必须再包含一个 `schema` 字段，该字段递归描述了该对象数组中的对象字段
3. is_option：可选字段，类型为数字，默认为1
   1. 1为可选，0为必选
   1. 如果为0（必选），且在序列化的时候输入json中不包含该字段，就会报错



## 一个更复杂的例子
数据json：[https://gitee.com/xunmenglong/xproto/blob/master/example/doc.json](https://gitee.com/xunmenglong/xproto/blob/master/example/doc.json)
schema文件：[https://gitee.com/xunmenglong/xproto/blob/master/example/doc.schema.json](https://gitee.com/xunmenglong/xproto/blob/master/example/doc.schema.json)

## 使用xproto的代码例子
请参考代码当中的example/example.cpp

里面有初始化schema，序列化json，解析二进制并获取值的演示。

## 自解析支持
跟protobuf不一样，protobuf 如果要定义一个新的结构体的话，就必须重新写 `*.proto` 文件，并且重新编译发布上线。


`xproto` 支持自解析，要支持新的结构体的时候，只需要将新的 `schema` 文件告诉 `xproto` ，然后 `xproto` 就可以处理新的结构体的json了，该过程是不用编译和发布新版本的。


当然，如果你是一个服务，想要做到动态更新的话，还需要在服务框架中做一些外围工作来支持动态reload schema信息。


因为始终还是有一个 `schema` 文件reload的过程，所以自解析虽然能支持，但是相比 `json` 的完全自解析，还是需要多花一些成本的。


## schema变更兼容性支持
如果你要实现一个存储系统，总是可能会偶尔做一些数据库的schema变更的场景，尤其是存储系统中已经有一部分老格式的数据，如何考虑新格式和老格式的兼容性是一个问题。


在 `xproto` 中，针对不同的schema变更的场景，对 `schema` 文件的变更建议如下：

1. 新增字段：
   1. 最简单的场景，直接新增即可，但是要注意新增字段的 `idx` 值要递增上涨
   1. 修改完新的schema之后，发送给存储系统reload即可
   1. 新增字段过后对老数据和新数据的序列化和读取都能直接兼容
   1. 但是要注意，因为存储系统里已经有一些老数据，所以新增字段建议是可选字段，而非必选字段
2. 删除字段
   1. 直接将要删除的字段的 `is_option` 调整为1即可
   1. 不建议将该字段给删除了，并且将该字段之后的兄弟节点的 `idx` 依次--，因为这样就无法保证兼容性了，对库里已有老数据就无法读取了
   1. 带来的代价是序列化后的空间会浪费一个字节，因为 `xproto` 的实现原理，只要在schema中定义了，即使数据json中不存在该字段，也会占用一个字节的占位符
3. 更新字段
   1. 目前的设计，不支持更新字段的类型，比如从int转换为string，因为不知道对老数据如何处理，所以从xproto的角度无法处理字段更新
   1. 如果存储系统要做到兼容，就必须存储系统自己实现外围逻辑，即定义一个默认的数据转换逻辑（可能是默认值，也可能是atoi之类的），然后逐条数据重新执行，重新生成新的二进制数据并做存储
# 原理
## 基础序列化原理
由于我们要求极限的反序列化的读取性能，而最快的读取性能就是 `struct` 中的字段读取，因为一个 `struct` 对象的字段读取只需要一次的内存寻址即可获取到该字段的值，但是 `struct` 无法支持 `schema` 的变更。


`protobuf` 每次在反序列化二进制码的时候，都需要遍历所有字段并将字段对应的值加载到内存当中，即要获取某个字段的寻址次数是跟json字段总数成正比的。


而我设计的 `xproto` 是逼近极限的一种设计，将每个字段读取从一次寻址增加为二次寻址，即比 `struct` 多一次寻址。


下面我们来阐述一下 `xproto` 的序列化原理。


先直接看一个最简单的示例 `user.json` ：
```json
{
  "name": "mike",
  "age": 32
}
```
将这个json序列化的时候，我们会将内容分成 `head` 和 `body` 两部分， `head` 中是一个 `int[]` ，每个元素代表当前字段在 `body` 中的offset位置，body是一个 `char[]` ，如下图：
![image.png](docs/image/readme_1.png)
可以看到，我们在序列化的时候，并没有保存原始json当中的 `name` 和 `age` 的字段名，是因为我们设计 `xproto` 的时候预期是在存储系统/搜索引擎中使用的，在同一个表格当中，同一份数据的 `schema` 是完全一样的，所以没必要针对每个数据多重复存储key的名称信息，浪费存储。


那么如果我们在数据中不存在 `key` 值信息的话，跟数据库类似，我们就需要提前定义好一个数据的 `schema` 了， `xproto` 提供了一个定义 `schema` 的语法，该语法也是json格式，以如上的 `user.json` 为例子，schema定义如下：
```json
{
  "user": {
    "idx": 0,
    "type": "string"
  },
  "age": {
    "idx": 1,
    "type": "int"
  }
}
```
其中 `idx` 跟 `protobuf` 类似，也是一个从0自增的唯一id，在 `xproto` 中，主要是用来标识该字段在 `head` 数组中的下标；


其中 `type` 字段用来标识该字段的数据类型，目前支持：

1. int
1. uint
1. int64
1. uint64
1. double
1. string
1. object
1. array

这几种数据类型。这些类型中，其中 `array` 和 `object` 的支持实现起来要复杂一些，这两个类型跟json类似，可以嵌套，即支持任意 `json` 格式文件的序列化。


最后我们来看一下以 `user.json` 为例，我们得到了序列化后的二进制数据，如何来进行字段的访问：
```cpp
char * serialized_data = ...; // 从磁盘/内存/网络中获取到的二进制

// 定义属性获取的基本类
xprotogetter g;
// 根据输入的序列化数据来返回读取句柄
int * handler = g.init(serialized_data);
// 获取name的值
char * name = g.v1(handler, 0);
// 获取age的指针
int * age_ptr = (int *)g.v1(handler, 1);
// 获取age的值
int age = *(int *)g.v1(handler, 1);
```
## 对象字段的序列化
以如下的 `user.json` 为例：
```json
{
  "name": "mike",
  "age": 32,
  "school": {
    "name": "第一小学",
    "address": "第一大街23号"
  },
  "gender": "男"
}
```
该文件序列化后的结构如下：
![image.png](docs/image/readme_2.png)
mike(0, 5) 括号中的(0, 5)分别代表该字段在body当中的(offset, limit)，看起来比较清楚。


可以看到最大的改变是 `head` 中第三个字段 `school` 的指针并不指向了 `body` ，而是指向了 `head` 中的一个位置，这个位置就是 `school` 字段的对象在 `head` 中的位置。


:::info
school的指针的值为4，4是相对坐标，不是绝对坐标，相对user_head第一个字段的坐标。


假如school中的address字段也是一个object的话，那么address中的值会是2，而不是6。这是一个嵌套递归的设计。
:::


示例如下：
```json
{
  "name": "mike",
  "age": 32,
  "school": {
    "name": "第一小学",
    "address": {
      "country": "中国",
      "city": "武汉",
      "street": "第一大街23号"
    }
  },
  "gender": "男"
}
```
该json的序列化后的二进制长这样：
![image.png](docs/image/readme_3.png)


接下来我们来看看针对这种嵌套的对象数组，如何获取其中的某个值吧：
```cpp
char * serialized_data = ...; // 从磁盘/内存/网络中获取到的二进制

// 定义属性获取的基本类
xprotogetter g;
// 根据输入的序列化数据来返回读取句柄
int * handler = g.init(serialized_data);

// 下面我们通过集中方法来获取school.address.city的值
// 方法1：通过v3函数来获取，v3函数代表该次请求接受3个寻址参数
// 1. 第一个参数2，代表school在user中的idx
// 2. 第二个参数1，代表address在school中的idx
// 3. 第三个参数1, 代表city在address中的idx
char * city_str = g.v3(handler, 2, 1, 1);

// 方法2：我们也可以先获得school_handler, 再获取address_handler, 再获取city的值
// 获取shcool_handler
int * school_handler = g.o1(handler, 2);
// 获取address_handler
int * address_handler = g.o1(school_handler, 1);
// 获取city_str
char * city_str = g.v1(address_handler, 1);

// 方法3：我们也可以直接获得address_handler, 再获取city的值
int * address_handler = g.o2(handler, 2, 1);
char * city_str = g.v1(address_handler, 1);
```
## 数组字段的序列化
我们来看一下如下的一个示例json：
```json
{
  "name": "mike",
  "age": 32,
  "children": ["jack", "eric", "sam"],
  "gender": "男"
}
```
该json序列化后的结构长这样：
![image.png](docs/image/readme_4.png)
可以看到，跟上面讲到的 `对象` 字段类似， `children` 字段也没有指向 `body` ，而是指向了 `head` ，但是跟 `对象` 不一样的是，该指针指向了第2个格子，中间还留一个格子 `3` ，就是如上 `head` 当中的 `23` 和 `9` 之间的 `3` ，这个 `3` 代表的是该数组包含的元素个数，方便获取某个字段的长度并进行遍历。


下面我们来看一个更复杂的案例，即数组对象，json如下：
```json
{
  "name": "mike",
  "age": 32,
  "children": [
    {
      "name": "jack",
      "age": 10
    },
    {
      "name": "eric",
      "age": 7
    }
  ],
  "gender": "男"
}
```
该json序列化后的结果如下图：
![image.png](docs/image/readme_5.png)


从上图可以看到，可以看到如果是一个对象数组的话， `children` 指针指向的数组中包含的值就不再是指向body中的指针了，而是再次指向 `head` 中的一个指针，这样经过二次指针，指向的 `jack` 或者 `eric` 头部指针的地址，才保存有真正指向body的指针。


:::info
思考问题：为什么在children_head中要做间接指针呢？在如上case中，我head设置为0 5 5 27 2 9 14 18 23的话，有什么问题？
:::
:::warning
答案：原因在于每一个数组中的object的head长度可能并不一样，比如object中又嵌套有数组的场景。
:::


接下来，我们来看一下，如何遍历打印所有儿子的名字：
```cpp
char * serialized_data = ...; // 从磁盘/内存/网络中获取到的二进制

// 定义属性获取的基本类
xprotogetter g;
// 根据输入的序列化数据来返回读取句柄
int * handler = g.init(serialized_data);

// 获得该对象的children的个数
int children_num = g.l1(handler, 2);
// 获得数组对象的句柄
int * children_handler = g.o1(handler, 2);
for (int i=0; i<children_num; i++) {
    // 获得当前对象的句柄
    int * cur_child_handler = g.o1(children_handler, i);
    // 获得名字和age的信息
    char * name = g.v1(cur_child_handler, 0);
    int * age = (int *)g.v1(cur_child_handler, 1);
    
    // 也可以不取出cur_child_handler，直接获取值
    // char * name = g.v2(children_handler, i, 0);
}
```
# 性能数据对比
我们主要对比 `xproto` , `protobuf` , `json` 三种数据，然后分别选取了一个简单的json和一个复杂的json为例子来进行测试。


我们主要对比序列化后的空间，读取效率。


原始json长这样：
```json
{
    "title": "东阳市马宅中心幼儿园 殷护学情，感动你我他",
    "url": "http://www.dongyang.gov.cn/11330783K15043025N/14/bmdt/202006/t20200623_4654671_1.html",
    "time": 2020,
    "areas": ["重庆", "广东", "东阳"],
    "media_ids": [101, 103, 105, 188],
    "author": {
        "name": "北京本地新闻",
        "tags": ["网媒", "时事"],
        "basic_info": {
            "school": {
                "school_name": "清华大学",
                "location": "五道口"
            },
            "age": 30
        }
    },
    "followers": [
        {
            "nickname": "小狗在玩",
            "nickid": 10001,
            "topics": [
                {"topic_name": "流浪狗收藏", "topic_level": 125},
                {"topic_name": "我爱公益", "topic_level": 80}
                ],
            "nick_tags": ["公益", "宠物", "文化"]
        },
        {
            "nickname": "小猫Kitty",
            "nickid": 10002,
            "topics": [
                {"topic_name": "流浪猫", "topic_level": 125},
                {"topic_name": "大神公益", "topic_level": 80}
                ],
            "nick_tags": ["科技", "公益", "宠物"]
        }
    ]
}
```
这个doc是模拟一个网页的的数据结构，里面包含了一些基本的标题、URL、时间，以及作者、粉丝等信息。


先看空间大小：

|  技术方案  |  序列化后的大小  |
| --- | --- |
| xproto | 632 |
| protobuf | 415 |
| json | 793 |

可以看出，从序列化后占据空间来看，json占据空间最大，xproto其次，protobuf最小。


这个是合理的，毕竟protobuf并没有 `xproto` 额外的 `head` 开销，并且采用了 `Varints编码` 来节约数字占用的空间。


但是 `xproto` 的强项是读取性能，下面我们来看一下读取性能的数据分析，如下所有表格所有操作都是循环10万次统计，统计单位是微秒：



|  读取数据  |  xproto  |  protobuf  | json |
| --- | --- | --- | --- |
| 读取1个字段 | 0 | 154160 | 533096 |
| 读取3个字段 | 0 | 158383 | 546805 |
| 读取3个字段+遍历1个数组(3个值) | 231 | 162572 | 560524 |
| 读取3个字段+遍历2个数组 | 774 | 161745 | 586252 |
| 读取3个字段+遍历2个数组+1个对象 | 776 | 161097 | 614918 |
| 遍历3个字段+遍历2个数组+1个对象+1个对象数组（每个对象中一个字段） | 968 | 162462 | 653008 |
| 遍历3个字段+遍历2个数组+1个对象+1个对象数组（每个对象中2个字段+1个数组） | 1812 | 164884 | 786830 |
| 遍历所有字段 | 2351 | 165630 | 848764 |



可以看到，在读取性能方面， `xproto` 是随着读取字段越多而成正比上升的，而 `protobuf` 由于会在最开始就将二进制转换为自动生成的对象类，所以读1个字段和读全部字段的差别并不大，而 `json` 使用的是 `rapidjson` 库，是最慢的，只能拿来做对照。


即使读取全部字段的情况下， `proto` 性能也比 `protobuf` 性能高70倍，如果只读取有限的几个字段的话，读取性能会比 `protobuf` 要高700倍！
# 参考

1. protobuf官方网址。[https://developers.google.com/protocol-buffers](https://developers.google.com/protocol-buffers)
1. protobuf序列化原理。[https://blog.csdn.net/daaikuaichuan/article/details/105639884](https://blog.csdn.net/daaikuaichuan/article/details/105639884)
