/* ================================================================
*   Copyright (C) 2020 All rights reserved.
*
*   文件名称：demo.cpp
*   创 建 者：xunmenglong
*   创建日期：2020年12月06日
*   描    述：
*
================================================================ */


#ifndef DEMO_CPP
#define DEMO_CPP

#include "xutil.h"
using namespace xutil;

#pragma pack (4)
struct data_head_t {
    uint64_t key;
    bool is_valid : 1;
    uint body_len : 31;
};
#pragma pack()

int get_result() {
    return 1000*98566;
}

int main(int argc, char ** argv) {
    cout << sizeof(data_head_t) << endl;
    data_head_t d1;
    d1.key = 10001;
    d1.body_len = 15;

    cout << d1.is_valid << ", " << d1.body_len << endl;
    d1.body_len = get_result();
    cout << d1.is_valid << ", " << d1.body_len << endl;
    return 0;
}
#endif
