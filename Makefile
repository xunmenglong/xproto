include config.mk
CXXFLAGS+=$(CPPFLAGS) -std=c++0x -DNDEBUG -D__const__= -pipe -W -Wall -Wno-unused-parameter -fPIC -fno-omit-frame-pointer
ifeq ($(NEED_GPERFTOOLS), 1)
	CXXFLAGS+=-DBRPC_ENABLE_CPU_PROFILER
endif

# 基本配置信息
NEED_GPERFTOOLS=0
ROOT_PATH=../..
XML_DIR=$(ROOT_PATH)/xunmenglong
BRPC_PATH=$(ROOT_PATH)/third_party/BRPC
THIRD_PARTY_DIR=$(ROOT_PATH)/third_party

# 引用依赖库
## BRPC
HDRS+=$(BRPC_PATH)/output/include
LIBS+=$(BRPC_PATH)/output/lib

## util库
HDRS+=$(XML_DIR)/xutil/

## third_pary相关
XXHASH_PATH=$(THIRD_PARTY_DIR)/xxHash/
HDRS+=$(XXHASH_PATH)
LIBS+=$(XXHASH_PATH)

##### 引用依赖库结束 ######

HDRPATHS=$(addprefix -I, $(HDRS))
LIBPATHS=$(addprefix -L, $(LIBS))

# 链接相关配置
STATIC_LINKINGS += -lbrpc -lxxhash
LINK_OPTIONS = -Xlinker "-(" $^ -Wl,-Bstatic $(STATIC_LINKINGS) -Wl,-Bdynamic -Xlinker "-)" $(DYNAMIC_LINKINGS)

ALL_CPP_FILE=$(wildcard *.cpp)
ALL_OBJS=$(ALL_CPP_FILE:.cpp=.o)

LIB=libxproto.a

.PHONY:all
all: $(LIB) output

.PHONY:output
output:
	mkdir -p output
	cp *.h output/
	cp *.a output/

%.o:%.cpp
	@echo "> Compiling $@"
	$(CXX) -c $(HDRPATHS) $(CXXFLAGS) $< -o $@

$(LIB) : $(ALL_OBJS)
	@echo "> Linking $@"
	rm -f $@
	ar cr $@ $<

.PHONY:clean
clean:
	@echo "> Cleaning"
	rm -rf *.o output $(LIB)
